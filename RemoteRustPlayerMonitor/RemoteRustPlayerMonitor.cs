﻿// Requires: RemoteRustCore

using ConVar;
using System.Collections.Generic;
using Newtonsoft.Json;
using Oxide.Core.Plugins;
using UnityEngine;

namespace Oxide.Plugins
{
    [Info("RemoteRust Server Player Monitor", "AnExiledGod", "0.3.0")]
    [Description("")]
    internal class RemoteRustPlayerMonitor : CovalencePlugin
    {
        [PluginReference] private Plugin RemoteRust;
        [PluginReference] private Plugin RemoteRustCore;

        private Timer _playerLocationTimer;

        #region Initialization

        private void Loaded()
        {
            
        }

        private void Unload()
        {
            _playerLocationTimer?.Destroy();
        }

        private void OnServerInitialized()
        {
            _playerLocationTimer = timer.Repeat(10f, 0, PlayerLocationHandler);
        }

        #endregion
        
        #region Hooks
        
        [HookMethod("GetVersion")]
        private string GetVersionHook()
        {
            return Version.ToString();
        }
        
        #endregion

        #region Player Location Tracking

        private class PlayerLocation
        {
            public ulong SteamID;
            public Vector3 Position;
            public string BodyRotation;
        }

        private class PlayerLocationData : RemoteRustCore.CoreData
        {
            public string Type = "PlayerLocations";
            public List<PlayerLocation> Locations;
        }

        private void PlayerLocationHandler()
        {
            ListHashSet<BasePlayer> activePlayers = BasePlayer.activePlayerList;

            if (activePlayers.Count == 0)
            {
                return;
            }

            List<PlayerLocation> playerLocations = new List<PlayerLocation>();

            foreach (BasePlayer player in activePlayers)
            {
                playerLocations.Add(new PlayerLocation()
                {
                    SteamID = player.userID,
                    Position = player.transform.position,
                    BodyRotation = player.eyes.bodyRotation.ToString()
                });
            }

            PlayerLocationData payload = new PlayerLocationData()
            {
                Locations = playerLocations
            };

            RemoteRust.Call("AddMessageToQueue", JsonConvert.SerializeObject(payload));
        }

        #endregion

        #region OnPlayerChat

        private class OnPlayerChatData : RemoteRustCore.CoreData
        {
            public string Type = "OnPlayerChat";
            public ulong SteamID;
            public string Channel;
            public string Message;
            public Vector3 Location;
        }

        private void OnPlayerChatHandler(BasePlayer player, string chatMessage, Chat.ChatChannel channel)
        {
            OnPlayerChatData payload = new OnPlayerChatData
            {
                SteamID = player.Connection.userid,
                Channel = channel.ToString(),
                Message = chatMessage,
                Location = player.transform.position
            };

            RemoteRust.Call("AddMessageToQueue", JsonConvert.SerializeObject(payload));
        }

        object OnPlayerChat(BasePlayer player, string message, Chat.ChatChannel channel)
        {
            OnPlayerChatHandler(player, message, channel);

            return null;
        }

        #endregion

        #region OnPlayerConnected

        private class OnPlayerConnectedData : RemoteRustCore.CoreData
        {
            public string Type = "OnPlayerConnected";
            public ulong SteamID;
            public string DisplayName;
            public string OperatingSystem;
            public string IPAddress;
            public uint AuthLevel;
            public Vector3 Location;
        }

        private void OnPlayerConnectedHandler(BasePlayer player)
        {
            OnPlayerConnectedData payload = new OnPlayerConnectedData
            {
                SteamID = player.userID,
                DisplayName = player.displayName,
                OperatingSystem = player.net.connection.os,
                IPAddress = player.net.connection.ipaddress,
                AuthLevel = player.net.connection.authLevel,
                Location = player.transform.position
            };

            RemoteRust.Call("AddMessageToQueue", JsonConvert.SerializeObject(payload));
        }

        void OnPlayerConnected(BasePlayer player)
        {
            OnPlayerConnectedHandler(player);
        }

        #endregion

        #region OnPlayerDeath

        private class OnPlayerDeathData : RemoteRustCore.CoreData
        {
            public string Type = "OnPlayerDeath";
            public ulong KillerID;
            public ulong KilledID;
            public string HitBoneName;
            public float ProjectileDistance;
            public int ProjectileID;
            public string ProjectilePrefab;
            public string WeaponName;
            public Vector3 Location;
        }

        private void OnPlayerDeathHandler(BasePlayer player, HitInfo info)
        {
            if (player.IsNpc)
            {
                return;
            }
            
            OnPlayerDeathData payload = new OnPlayerDeathData
            {
                KillerID = player.OwnerID,
                KilledID = info.HitEntity != null ? info.HitEntity.OwnerID : 0,
                HitBoneName = info.boneName,
                ProjectileDistance = info.ProjectileDistance,
                ProjectileID = info.ProjectileID,
                ProjectilePrefab = info.ProjectilePrefab != null ? info.ProjectilePrefab.name : null,
                WeaponName = info.Weapon != null ? info.Weapon.name : null,
                Location = player.transform.position
            };

            RemoteRust.Call("AddMessageToQueue", JsonConvert.SerializeObject(payload));
        }

        void OnPlayerDeath(BasePlayer player, HitInfo info)
        {
            OnPlayerDeathHandler(player, info);

            return;
        }

        #endregion

        #region OnPlayerDisconnected

        private class OnPlayerDisconnectedData : RemoteRustCore.CoreData
        {
            public string Type = "OnPlayerDisconnected";
            public ulong SteamID;
            public string Reason;
            public Vector3 Location;
        }

        private void OnPlayerDisconnectedHandler(BasePlayer player, string reason)
        {
            OnPlayerDisconnectedData payload = new OnPlayerDisconnectedData
            {
                SteamID = player.userID,
                Reason = reason,
                Location = player.transform.position
            };

            RemoteRust.Call("AddMessageToQueue", JsonConvert.SerializeObject(payload));
        }

        void OnPlayerDisconnected(BasePlayer player, string reason)
        {
            OnPlayerDisconnectedHandler(player, reason);

            return;
        }

        #endregion
        
        #region OnPlayerCommand

        private class OnPlayerCommandData : RemoteRustCore.CoreData
        {
            public string Type = "OnPlayerCommand";
            public ulong SteamID;
            public string Command;
            public string Arguments;
            public Vector3 Location;
        }

        private void OnPlayerCommandHandler(BasePlayer player, string command, string[] args)
        {
            OnPlayerCommandData payload = new OnPlayerCommandData
            {
                SteamID = player.OwnerID,
                Command = command,
                Arguments = args.ToString(),
                Location = player.transform.position
            };
            
            RemoteRust.Call("AddMessageToQueue", JsonConvert.SerializeObject(payload));
        }
        
        void OnPlayerCommand(BasePlayer player, string command, string[] args)
        {
            OnPlayerCommandHandler(player, command, args);
        }
        
        #endregion
    }
}